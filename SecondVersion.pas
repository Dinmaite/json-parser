unit SecondVersion;

interface

uses
  System.Generics.Collections, SysUtils, Vcl.ComCtrls;

type
  TStringArray = array of string;

  TJsonObject = class
  private
    FMaxDepth: integer;

    function FindCloseSymbolIndex(body: string): integer;

    function AddParameter(const Value: string; var ArrayIndex: integer)
      : integer; // Returns param length
    function AddObject(const Name, Value: string;
      var ArrayIndex: integer): integer;
    // Returns object length
    function AddArray(const Name, Value: string;
      var ArrayIndex: integer): integer;
    // Returns arrayC length

    function JsonTextDecode(const Text: string): string;

    procedure SetParams(TreeView: TTreeView; TreeNode: TTreeNode;
      const Obj: TDictionary<string, string>);
    procedure SetArrays(TreeView: TTreeView; TreeNode: TTreeNode;
      const Obj: TObjectDictionary<string, TJsonObject>);
    procedure SetObjects(TreeView: TTreeView; TreeNode: TTreeNode;
      const Obj: TObjectDictionary<string, TJsonObject>);

  public
    JParams: TDictionary<string, string>;
    JObjs: TObjectDictionary<string, TJsonObject>;
    JArray: TObjectDictionary<string, TJsonObject>;

    procedure SendToTreeView(TreeView: TTreeView);

    constructor Create(const body: string; MaxNestedDepth: integer;
    IsStart: boolean = false);
    destructor Destroy; override;
  end;

implementation

{ TJsonObject }

function TJsonObject.AddArray(const Name, Value: string;
  var ArrayIndex: integer): integer;
var
  CloseIndex: integer;
  Temp, LName: string;
begin
  LName := Copy(Name, Name.IndexOf('"') + 2,
    Name.IndexOf('"', Name.IndexOf('"') + 2) - 1);
  LName := LName.Replace('"', '');
  LName := LName.Replace(':', '');
  Temp := Value;

  if JArray = nil then
    JArray := TObjectDictionary<string, TJsonObject>.Create([doOwnsValues]);
  CloseIndex := FindCloseSymbolIndex(Value);

  if LName.IsEmpty then // Temp is empty when it is array
    LName := IntToStr(ArrayIndex);
  inc(ArrayIndex);
  JArray.AddOrSetValue(LName, TJsonObject.Create(Copy(Temp, 2,
    CloseIndex - 1), FMaxDepth));

  Result := CloseIndex;
end;

function TJsonObject.AddObject(const Name, Value: string;
  var ArrayIndex: integer): integer;
var
  CloseIndex: integer;
  Temp, LName: string;
begin
  LName := Copy(Name, Name.IndexOf('"') + 2,
    Name.IndexOf('"', Name.IndexOf('"') + 2) - 1);
  LName := LName.Replace('"', '');
  LName := LName.Replace(':', '');
  Temp := Value;

  if JObjs = nil then
    JObjs := TObjectDictionary<string, TJsonObject>.Create([doOwnsValues]);
  CloseIndex := FindCloseSymbolIndex(Value);

  if LName.IsEmpty then // Temp is empty when it is array
    LName := IntToStr(ArrayIndex);
  inc(ArrayIndex);
  JObjs.AddOrSetValue(LName, TJsonObject.Create(Copy(Temp, 2, CloseIndex - 1), FMaxDepth));

  Result := CloseIndex;
end;

function TJsonObject.AddParameter(const Value: string;
  var ArrayIndex: integer): integer;
var
  I, J: integer;
  VKey, VValue: string;
begin
  Result := - 1;

  if Value.IsEmpty then
    exit;

  VKey := '';
  VValue := '';

  I := Value.IndexOf('"') + 1;

  while I < Value.Length do
    begin
      inc(I);
      if Value[I] = '"' then
        break;
      VKey := VKey + Value[I];
    end;

  if not Value.Contains('"') then
    begin
      VValue := VKey;
      VKey := IntToStr(ArrayIndex);
      inc(ArrayIndex);
    end;

  inc(I);
  if Value[I] = ':' then
    begin
      inc(I);
      while I < Value.Length do
        begin
          inc(I);
          if (Value[I] = ',') then
            break;
          VValue := VValue + Value[I];
        end;
    end;

  VKey := VKey.Replace('"', '');
  for J := 1 to VValue.Length - 1 do
    begin
      if VValue[j] = '"' then
        if VValue[j - 1] <> '\' then
          VValue := VValue.Remove(j - 1) + VValue.Substring(j)
        else
          VValue := VValue.Remove(j - 2) + VValue.Substring(j - 1)
    end;
  //VValue := VValue.Replace('"', '');

  if VValue.Contains('\u') then
    VValue := JsonTextDecode(VValue);

  if JParams = nil then
    JParams := TDictionary<string, string>.Create;

  JParams.AddOrSetValue(VKey, VValue);
  Result := 1;
end;

constructor TJsonObject.Create(const body: string; MaxNestedDepth: integer;
    IsStart: boolean = false);
var
  I, ArrayIndex: integer;
  tempbody, Temp: string;
  IsString: boolean;
  IterationCounter, ObjectLength: integer;
begin
  inherited Create;
  IterationCounter := 0;
  ArrayIndex := 0;
  tempbody := body;
  IsString := false;
  if IsStart then
    I := 1
  else
    I := 0;

  FMaxDepth := MaxNestedDepth;
  Dec(FMaxDepth);
  if FMaxDepth < 0 then
    exit;

  while I < tempbody.Length do
    begin
      inc(I);
      inc(IterationCounter);
      if IterationCounter > 10000 then
        break;

      if tempbody[I] = '"' then
        if tempbody[I - 1] <> '\' then
          IsString := not IsString;
      if not IsString then
        case tempbody[I] of
          '{':
            begin
              if tempbody[I - 1] = '\' then
                Continue;
              ObjectLength :=
                AddObject(Temp, Copy(tempbody, I, tempbody.Length), ArrayIndex);
              if ObjectLength > 1 then
                I := I + ObjectLength;
              Temp := '';
            end;
          '[': // I := I + AddArray(Temp, Copy(tempbody, I, tempbody.Length));
            begin
              if tempbody[I - 1] = '\' then
                Continue;
              ObjectLength := AddArray(Temp, Copy(tempbody, I, tempbody.Length),
                ArrayIndex);
              if ObjectLength > 1 then
                I := I + ObjectLength;
              Temp := '';
            end;
        else
          if (tempbody[I] = ',') or (tempbody[I] = ']') or (tempbody[I] = '}')
          then
            begin // ����� ���������
              I := I + AddParameter(Temp, ArrayIndex);
              Temp := '';
            end
        end;
      // if (tempbody[I] <> '{') and (tempbody[I] <> '[') then
      Temp := Temp + tempbody[I];
    end;
end;

destructor TJsonObject.Destroy;
begin
  if JObjs <> nil then
    JObjs.Free;
  if JArray <> nil then
    JArray.Free;
  if JParams <> nil then
    JParams.Free;
  inherited;
end;

function TJsonObject.FindCloseSymbolIndex(body: string): integer;
var
  Index: integer;
  I: integer;
  CloseSymbol, OpenSymbol: Char;
  IsString: boolean;
begin
  index := 0;
  Result := - 1;
  IsString := false;
  if body.IsEmpty then
    exit;

  OpenSymbol := body[1];
  case OpenSymbol of
    '{':
      CloseSymbol := '}';
    '[':
      CloseSymbol := ']';
    else
      CloseSymbol := #0;
  end;

  for I := 1 to body.Length do
    begin
      if body[I] = '"' then
        if (body[I - 1] <> '\') or (body[I - 2] + body[I - 1]  = '\\') then
          IsString := not IsString;
      if IsString then
        Continue;
      if body[I] = OpenSymbol then
        begin
          inc(index);
        end;
      if body[I] = CloseSymbol then
        begin
          dec(Index);
        end;
      if Index = 0 then
        exit(I);
    end;
end;

function TJsonObject.JsonTextDecode(const Text: string): string;
var
  Temp, FirstPart, SecondPart, symbol: string;
begin
  Temp := Text;
  while Temp.Contains('\u') do
    begin
      FirstPart := Temp.Split(['\u'])[0];
      SecondPart := Copy(Temp, Length(FirstPart) + 1,
        Length(Temp) - Length(FirstPart));
      symbol := Copy(SecondPart, 3, 4);
      delete(SecondPart, 1, 6);
      Temp := FirstPart + chr(strtoint('$' + symbol)) + SecondPart;
    end;
  Result := Temp;
end;

procedure TJsonObject.SendToTreeView(TreeView: TTreeView);
var
  Node: TTreeNode;
begin
  if TreeView = nil then
    exit;
  Node := TreeView.Items.Add(nil, 'Root');
  SetParams(TreeView, Node, self.JParams);
  SetArrays(TreeView, Node, self.JArray);
  SetObjects(TreeView, Node, self.JObjs);
end;

procedure TJsonObject.SetArrays(TreeView: TTreeView; TreeNode: TTreeNode;
  const Obj: TObjectDictionary<string, TJsonObject>);
var
  I: integer;
  Node: TTreeNode;
begin
  if Obj <> nil then
    for I := 0 to Obj.Count - 1 do
      begin
        Node := TreeView.Items.AddChild(TreeNode,
          '[' + Obj.ToArray[I].Key + ']');
        SetParams(TreeView, Node, Obj.ToArray[I].Value.JParams);
        SetArrays(TreeView, Node, Obj.ToArray[I].Value.JArray);
        SetObjects(TreeView, Node, Obj.ToArray[I].Value.JObjs);
      end;
end;

procedure TJsonObject.SetObjects(TreeView: TTreeView; TreeNode: TTreeNode;
  const Obj: TObjectDictionary<string, TJsonObject>);
var
  I: integer;
  Node: TTreeNode;
begin
  if Obj <> nil then
    for I := 0 to Obj.Count - 1 do
      begin
        Node := TreeView.Items.AddChild(TreeNode,
          '{' + Obj.ToArray[I].Key + '}');
        SetParams(TreeView, Node, Obj.ToArray[I].Value.JParams);
        SetArrays(TreeView, Node, Obj.ToArray[I].Value.JArray);
        SetObjects(TreeView, Node, Obj.ToArray[I].Value.JObjs);
      end;
end;

procedure TJsonObject.SetParams(TreeView: TTreeView; TreeNode: TTreeNode;
  const Obj: TDictionary<string, string>);
var
  I: integer;
begin
  if Obj <> nil then
    for I := 0 to Obj.Count - 1 do
      TreeView.Items.AddChild(TreeNode, Obj.ToArray[I].Key + '=' +
        Obj.ToArray[I].Value)
end;

end.
