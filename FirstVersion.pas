unit FirstVersion;

interface

uses
  System.Generics.Collections, SysUtils, Vcl.ComCtrls;

type
  TStringArray = array of string;

  TJsonObject = class
  private
    function FindCloseSymbolIndex(body: string): integer;

    procedure SetParams(TreeView: TTreeView; TreeNode: TTreeNode;
      const Obj: TDictionary<string, string>);
    procedure SetArrays(TreeView: TTreeView; TreeNode: TTreeNode;
      const Obj: TObjectDictionary<string, TJsonObject>);
    procedure SetObjects(TreeView: TTreeView; TreeNode: TTreeNode;
      const Obj: TObjectDictionary<string, TJsonObject>);

  public
    JParams: TDictionary<string, string>;
    JObjs: TObjectDictionary<string, TJsonObject>;
    JArray: TObjectDictionary<string, TJsonObject>;

    procedure SendToTreeView(TreeView: TTreeView);

    constructor Create(body: string);
    destructor Destroy; override;
  end;

implementation

{ TJsonObject }

constructor TJsonObject.Create(body: string);
var
  I, CloseIndex, ArrayIndex: integer;
  tempbody, Temp, Key, Value: string;
  IsString: boolean;
begin
  inherited Create;
  ArrayIndex := 0;
  tempbody := body;
  IsString := False;

  I := 1;

  while I <= Length(tempbody) do
    begin
      inc(I);

            if tempbody[I] = '"' then
        IsString := not IsString;
      if not IsString then

      case tempbody[I] of
        '{':
          begin
            if JObjs = nil then
              JObjs := TObjectDictionary<string, TJsonObject>.Create
                ([doOwnsValues]);
            CloseIndex := FindCloseSymbolIndex
              (copy(tempbody, I, tempbody.Length));

            Temp := Temp.Replace(':', '');
            Temp := Temp.Replace('"', '');
            Temp := Temp.Replace(' ', '');
            if Temp.IsEmpty then // Temp is empty when it is array
              Temp := IntToStr(ArrayIndex);
            inc(ArrayIndex);
            JObjs.AddOrSetValue(Temp, TJsonObject.Create(copy(tempbody, I,
              CloseIndex)));
            I := I + CloseIndex;
            Temp := '';
          end;

        '[':
          begin
            if JArray = nil then
              JArray := TObjectDictionary<string, TJsonObject>.Create
                ([doOwnsValues]);
            CloseIndex := FindCloseSymbolIndex
              (copy(tempbody, I, tempbody.Length));

            Temp := Temp.Replace(':', '');
            Temp := Temp.Replace('"', '');
            Temp := Temp.Replace(' ', '');
            if Temp.IsEmpty then // Temp is empty when it is array
              Temp := IntToStr(ArrayIndex);
            inc(ArrayIndex);
            JArray.AddOrSetValue(Temp,
              TJsonObject.Create(copy(tempbody, I, CloseIndex)));
            I := I + CloseIndex;
            Temp := '';
          end
      else
        if (tempbody[I] = ',') or (tempbody[I] = '}') or (tempbody[I] = ']')
        then
          begin
            if not Temp.Contains('": ') then
              // begin
              // if (Temp.Length < 2) then
              Continue;
            // temp := ' ":  ' + temp;  //����� ��� ������
            // end;
            if Temp[Temp.Length] = ':' then
              Temp := Temp.Replace(':', '":  ');
            if JParams = nil then
              JParams := TDictionary<string, string>.Create;
            Key := Temp.Split(['": '])[0];
            Key := Key.Replace('"', '');
            Key := Key.Replace(' ', '');

            Value := Temp.Split(['": '])[1];
            Value := Value.Replace('"', '');
            Value := Value.Replace(' ', '');
            JParams.AddOrSetValue(Key, Value);
            Temp := '';
          end
       // else
        //  Temp := Temp + tempbody[I];
      end;
      Temp := Temp + tempbody[I];//
    end;
end;

destructor TJsonObject.Destroy;
begin
  if JObjs <> nil then
    JObjs.Free;
  if JArray <> nil then
    JArray.Free;
  if JParams <> nil then
    JParams.Free;
  inherited;
end;

function TJsonObject.FindCloseSymbolIndex(body: string): integer;
var
  Index: integer;
  I: integer;
  CloseSymbol, OpenSymbol: Char;
  IsString: boolean;
begin
  index := 0;
  Result := - 1;
  IsString := False;
  if body.IsEmpty then
    exit;

  OpenSymbol := body[1];
  case OpenSymbol of
    '{':
      CloseSymbol := '}';
    '[':
      CloseSymbol := ']';
    else
      CloseSymbol := #0;
  end;

  for I := 1 to body.Length do
    begin
    if body[I] = '"' then
        IsString := not IsString;
      if IsString then
        Continue;
      if body[I] = OpenSymbol then
        inc(index);
      if body[I] = CloseSymbol then
        dec(Index);
      if Index = 0 then
        exit(I);
    end;
end;

procedure TJsonObject.SendToTreeView(TreeView: TTreeView);
var
  Node: TTreeNode;
begin
  if TreeView = nil then
    exit;
  Node := TreeView.Items.Add(nil, 'Root');
  SetParams(TreeView, Node, self.JParams);
  SetArrays(TreeView, Node, self.JArray);
  SetObjects(TreeView, Node, self.JObjs);
end;

procedure TJsonObject.SetArrays(TreeView: TTreeView; TreeNode: TTreeNode;
  const Obj: TObjectDictionary<string, TJsonObject>);
var
  I: integer;
  Node: TTreeNode;
begin
  if Obj <> nil then
    for I := 0 to Obj.Count - 1 do
      begin
        Node := TreeView.Items.AddChild(TreeNode,
          '[' + Obj.ToArray[I].Key + ']');
        SetParams(TreeView, Node, Obj.ToArray[I].Value.JParams);
        SetArrays(TreeView, Node, Obj.ToArray[I].Value.JArray);
        SetObjects(TreeView, Node, Obj.ToArray[I].Value.JObjs);
      end;
end;

procedure TJsonObject.SetObjects(TreeView: TTreeView; TreeNode: TTreeNode;
  const Obj: TObjectDictionary<string, TJsonObject>);
var
  I: integer;
  Node: TTreeNode;
begin
  if Obj <> nil then
    for I := 0 to Obj.Count - 1 do
      begin
        Node := TreeView.Items.AddChild(TreeNode,
          '{' + Obj.ToArray[I].Key + '}');
        SetParams(TreeView, Node, Obj.ToArray[I].Value.JParams);
        SetArrays(TreeView, Node, Obj.ToArray[I].Value.JArray);
        SetObjects(TreeView, Node, Obj.ToArray[I].Value.JObjs);
      end;
end;

procedure TJsonObject.SetParams(TreeView: TTreeView; TreeNode: TTreeNode;
  const Obj: TDictionary<string, string>);
var
  I: integer;
begin
  if Obj <> nil then
    for I := 0 to Obj.Count - 1 do
      TreeView.Items.AddChild(TreeNode, Obj.ToArray[I].Key + '=' +
        Obj.ToArray[I].Value)
end;

end.
