unit JSONObject;

interface

uses SysUtils, Generics.Collections, VCL.ComCtrls;

type
  TJSONObject = class
  private
    FMaxNestedDepth: Integer;

    function AddObject(const Key, Value: string;
      var ArrayIndex: Integer): Integer;
    function AddArray(const Key, Value: string;
      var ArrayIndex: Integer): Integer;
    function AddParameter(const Key, Value: string;
      var ArrayIndex: Integer): Integer;

    function FindCloseSymbolIndex(const Value: string): Integer;
    function JsonTextDecode(const Text: string): string;

    procedure InternalParsJSON(const JSONString: string;
      const MaxNestedDepth: Integer = 30);

    // Fill TreeView
    procedure SetParams(TreeView: TTreeView; TreeNode: TTreeNode;
      const Obj: TDictionary<string, string>);
    procedure SetArrays(TreeView: TTreeView; TreeNode: TTreeNode;
      const Obj: TObjectDictionary<string, TJSONObject>);
    procedure SetObjects(TreeView: TTreeView; TreeNode: TTreeNode;
      const Obj: TObjectDictionary<string, TJSONObject>);
  public
    JParams: TDictionary<string, string>;
    JObjs: TObjectDictionary<string, TJSONObject>;
    JArray: TObjectDictionary<string, TJSONObject>;

    function GetValue(const Path: string): string;
    procedure SendToTreeView(TreeView: TTreeView);

    function ParsJSON(const JSONString: string;
      const MaxNestedDepth: Integer = 30): Boolean;

    constructor Create;
    destructor Destroy; override;
  end;

implementation

{ TJSONObject }

function TJSONObject.AddArray(const Key, Value: string;
  var ArrayIndex: Integer): Integer;
var
  CloseIndex: Integer;
  Temp, LName: string;
  TempObject: TJSONObject;
begin
  LName := Key; { Copy(Key, Key.IndexOf('"') + 2,
    Key.IndexOf('"', Key.IndexOf('"') + 2) - 1);
    LName := LName.Replace('"', '');
    LName := LName.Replace(':', ''); }
  Temp := Value;

  if JArray = nil then
    JArray := TObjectDictionary<string, TJSONObject>.Create([doOwnsValues]);
  CloseIndex := FindCloseSymbolIndex(Value);

  if LName.IsEmpty then // Temp is empty when it is array
    LName := IntToStr(ArrayIndex);
  inc(ArrayIndex);

  TempObject := TJSONObject.Create;
  TempObject.InternalParsJSON(Copy(Temp, 2, CloseIndex - 1), FMaxNestedDepth);
  JArray.AddOrSetValue(LName, TempObject);

  Result := CloseIndex;
end;

function TJSONObject.AddObject(const Key, Value: string;
  var ArrayIndex: Integer): Integer;
var
  CloseIndex: Integer;
  Temp, LName: string;
  TempObject: TJSONObject;
begin
  LName := Key; { Copy(Name, Name.IndexOf('"') + 2,
    Name.IndexOf('"', Name.IndexOf('"') + 2) - 1);
    LName := LName.Replace('"', '');
    LName := LName.Replace(':', ''); }
  Temp := Value;

  if JObjs = nil then
    JObjs := TObjectDictionary<string, TJSONObject>.Create([doOwnsValues]);
  CloseIndex := FindCloseSymbolIndex(Value);

  if LName.IsEmpty then // Temp is empty when it is array
  begin
    LName := IntToStr(ArrayIndex);
    inc(ArrayIndex);
  end;

  TempObject := TJSONObject.Create;
  TempObject.InternalParsJSON(Copy(Temp, 2, CloseIndex - 1), FMaxNestedDepth);
  JObjs.AddOrSetValue(LName, TempObject);

  Result := CloseIndex;
end;

function TJSONObject.AddParameter(const Key, Value: string;
  var ArrayIndex: Integer): Integer;
var
  LKey, LValue: string;
begin
  if JParams = nil then
    JParams := TDictionary<string, string>.Create;

  if Key.IsEmpty then
  begin
    LKey := ArrayIndex.ToString;
    inc(ArrayIndex);
  end
  else
    LKey := Key;

  // if string
  if not Value.IsEmpty then
    if (Value[1] = '"') and (Value[Value.Length] = '"') then
    begin
      LValue := Value.Remove(0, 1);
      LValue := LValue.Remove(LValue.Length - 1, 1);
      LValue := JsonTextDecode(LValue);
    end
    else
      LValue := JsonTextDecode(Value);

  JParams.AddOrSetValue(LKey, LValue);
  Result := 0;
end;

constructor TJSONObject.Create;
begin
  inherited Create;
end;

destructor TJSONObject.Destroy;
begin
  JParams.Free;
  JObjs.Free;
  JArray.Free;
  inherited;
end;

function TJSONObject.FindCloseSymbolIndex(const Value: string): Integer;
var
  Index: Integer;
  I: Integer;
  CloseSymbol, OpenSymbol: Char;
  IsString: Boolean;
begin
  Index := 0;
  Result := -1;
  IsString := false;
  if Value.IsEmpty then
    Exit;

  OpenSymbol := Value[1];
  case OpenSymbol of
    '{':
      CloseSymbol := '}';
    '[':
      CloseSymbol := ']';
  else
    CloseSymbol := #0;
  end;

  for I := 1 to Value.Length do
  begin
    if Value[I] = '"' then
      if (Value[I - 1] <> '\') or (Value[I - 2] + Value[I - 1] = '\\') then
        IsString := not IsString;

    if IsString then
      Continue;
    if Value[I] = OpenSymbol then
      inc(Index);

    if Value[I] = CloseSymbol then
      Dec(Index);

    if Index = 0 then
      Exit(I);
  end;
end;

function TJSONObject.GetValue(const Path: string): string;
var
  LPath: TArray<string>;
  Node: TJSONObject;
  I: Integer;
begin
  LPath := Path.Split(['\']);

  Node := Self;
  for I := 0 to Length(LPath) - 1 do
  begin
    if Node.JObjs <> nil then
      if Node.JObjs.ContainsKey(LPath[I]) then
      begin
        Node := Node.JObjs[LPath[I]];
        Continue;
      end;

    if Node.JArray <> nil then
      if Node.JArray.ContainsKey(LPath[I]) then
      begin
        Node := Node.JArray[LPath[I]];
        Continue;
      end;

    if Node.JParams <> nil then
      if Node.JParams.ContainsKey(LPath[I]) then
        Result := Node.JParams[LPath[I]];
  end;

  SetLength(LPath, 0);
end;

procedure TJSONObject.InternalParsJSON(const JSONString: string;
  const MaxNestedDepth: Integer);
var
  ArrayIndex, I, ObjLength: Integer;
  LJSONString, TempJSONString, KeyName: string;
  IsString: Boolean;
begin
  ArrayIndex := 0;
  LJSONString := JSONString;
  IsString := false;

  FMaxNestedDepth := MaxNestedDepth;
  Dec(FMaxNestedDepth);
  if FMaxNestedDepth < 1 then
    Exit;

  I := 0;
  TempJSONString := '';

  while I < LJSONString.Length do
  begin
    inc(I);

    if (LJSONString[I] = '"') then // if start or stop string
      if (LJSONString[I - 1] <> '\') then // check for escape symbol
      begin
        if IsString then
          // TempJSONString := TempJSONString + JSONString[I] // Add last quote
        else
          TempJSONString := '';
        IsString := not IsString;
      end;

    if IsString then
      TempJSONString := TempJSONString + LJSONString[I]
    else
      case LJSONString[I] of
        ':':
          begin
            KeyName := TempJSONString.Replace('"', '');
            // Key not contains quotes
            TempJSONString := '';

          end;
        '{':
          begin
            if LJSONString[I - 1] = '\' then // check for escape symbol
              Continue;

            ObjLength := AddObject(KeyName,
              Copy(LJSONString, I, LJSONString.Length), ArrayIndex);
            if ObjLength > 1 then
              I := I + ObjLength;
            TempJSONString := '';
            // break;
          end;
        '[': // I := I + AddArray(Temp, Copy(tempbody, I, tempbody.Length));
          begin
            if LJSONString[I - 1] = '\' then
              Continue;
            ObjLength := AddArray(KeyName,
              Copy(LJSONString, I, LJSONString.Length), ArrayIndex);
            if ObjLength > 1 then
              I := I + ObjLength;
            TempJSONString := '';
            // break;
          end;
      else
        if (LJSONString[I] = ',') or (LJSONString[I] = ']') or
          (LJSONString[I] = '}') then
        begin // ����� ���������
          I := I + AddParameter(KeyName, TempJSONString, ArrayIndex);
          TempJSONString := '';
        end
        else
          if (LJSONString[I] <> #9) and (LJSONString[I] <> ' ') and
            (LJSONString[I] <> #13) and (LJSONString[I] <> #10) then
            TempJSONString := TempJSONString + LJSONString[I];
      end;

  end;

end;

function TJSONObject.JsonTextDecode(const Text: string): string;
var
  Temp, FirstPart, SecondPart, Symbol: string;
begin
  Temp := Text;

  while Temp.Contains('\u') do
  begin
    FirstPart := Temp.Split(['\u'])[0];
    SecondPart := Copy(Temp, Length(FirstPart) + 1,
      Length(Temp) - Length(FirstPart));
    Symbol := Copy(SecondPart, 3, 4);
    Delete(SecondPart, 1, 6);
    Temp := FirstPart + Chr(StrToInt('$' + Symbol)) + SecondPart;
  end;

  Result := Temp;
end;

function TJSONObject.ParsJSON(const JSONString: string;
  const MaxNestedDepth: Integer = 30): Boolean;
var
  LJSONString: string;
begin
  LJSONString := JSONString;
  Delete(LJSONString, 1, 1);
  try
    InternalParsJSON(LJSONString, MaxNestedDepth);
    Result := True;
  except
    Result := false;
  end;
end;

procedure TJSONObject.SendToTreeView(TreeView: TTreeView);
var
  Node: TTreeNode;
begin
  if TreeView = nil then
    Exit;
  Node := TreeView.Items.Add(nil, 'Root');
  SetParams(TreeView, Node, Self.JParams);
  SetArrays(TreeView, Node, Self.JArray);
  SetObjects(TreeView, Node, Self.JObjs);
end;

procedure TJSONObject.SetArrays(TreeView: TTreeView; TreeNode: TTreeNode;
  const Obj: TObjectDictionary<string, TJSONObject>);
var
  I: Integer;
  Node: TTreeNode;
begin
  if Obj <> nil then
    for I := 0 to Obj.Count - 1 do
    begin
      Node := TreeView.Items.AddChild(TreeNode, '[' + Obj.ToArray[I].Key + ']');
      SetParams(TreeView, Node, Obj.ToArray[I].Value.JParams);
      SetArrays(TreeView, Node, Obj.ToArray[I].Value.JArray);
      SetObjects(TreeView, Node, Obj.ToArray[I].Value.JObjs);
    end;
end;

procedure TJSONObject.SetObjects(TreeView: TTreeView; TreeNode: TTreeNode;
  const Obj: TObjectDictionary<string, TJSONObject>);
var
  I: Integer;
  Node: TTreeNode;
begin
  if Obj <> nil then
    for I := 0 to Obj.Count - 1 do
    begin
      Node := TreeView.Items.AddChild(TreeNode, '{' + Obj.ToArray[I].Key + '}');
      SetParams(TreeView, Node, Obj.ToArray[I].Value.JParams);
      SetArrays(TreeView, Node, Obj.ToArray[I].Value.JArray);
      SetObjects(TreeView, Node, Obj.ToArray[I].Value.JObjs);
    end;
end;

procedure TJSONObject.SetParams(TreeView: TTreeView; TreeNode: TTreeNode;
  const Obj: TDictionary<string, string>);
var
  I: Integer;
begin
  if Obj <> nil then
    for I := 0 to Obj.Count - 1 do
      TreeView.Items.AddChild(TreeNode, Obj.ToArray[I].Key + '=' +
        Obj.ToArray[I].Value)
end;

end.
