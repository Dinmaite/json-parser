program Project1;

uses
  Vcl.Forms,
  MainForm in 'MainForm.pas' {FormMain},
  FirstVersion in 'FirstVersion.pas',
  SecondVersion in 'SecondVersion.pas',
  JSONObject in 'JSONObject.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFormMain, FormMain);
  Application.Run;
end.
