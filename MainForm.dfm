object FormMain: TFormMain
  Left = 0
  Top = 0
  Caption = 'Test Json Parser'
  ClientHeight = 494
  ClientWidth = 725
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    725
    494)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 117
    Height = 13
    Caption = 'Insert JSON string here:'
  end
  object Memo1: TMemo
    Left = 8
    Top = 27
    Width = 296
    Height = 369
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    WordWrap = False
  end
  object TreeView1: TTreeView
    Left = 311
    Top = 27
    Width = 406
    Height = 401
    Anchors = [akTop, akRight, akBottom]
    Indent = 19
    SortType = stText
    TabOrder = 1
    OnClick = TreeView1Click
    ExplicitHeight = 406
  end
  object Edit1: TEdit
    Left = 8
    Top = 442
    Width = 709
    Height = 21
    Anchors = [akLeft, akRight, akBottom]
    TabOrder = 2
  end
  object Button3: TButton
    Left = 8
    Top = 402
    Width = 297
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Pars'
    TabOrder = 3
    OnClick = Button3Click
  end
  object Edit2: TEdit
    Left = 8
    Top = 469
    Width = 628
    Height = 21
    Anchors = [akLeft, akRight, akBottom]
    TabOrder = 4
  end
  object Button4: TButton
    Left = 642
    Top = 469
    Width = 75
    Height = 21
    Anchors = [akRight, akBottom]
    Caption = 'Try get'
    TabOrder = 5
    OnClick = Button4Click
  end
end
