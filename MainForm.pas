unit MainForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, JSONObject,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.CategoryButtons,
  Vcl.ExtCtrls, Vcl.ComCtrls, Vcl.Menus;

type
  TFormMain = class(TForm)
    Memo1: TMemo;
    TreeView1: TTreeView;
    Edit1: TEdit;
    Label1: TLabel;
    Button3: TButton;
    Edit2: TEdit;
    Button4: TButton;
    procedure TreeView1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormMain: TFormMain;
  Root: TJsonObject;

implementation

{$R *.dfm}

uses DateUtils;

procedure TFormMain.Button3Click(Sender: TObject);
begin
  TreeView1.Items.Clear;
  if Root <> nil then
    Root.Free;

  Root := TJsonObject.Create;
  Root.ParsJSON(Memo1.Text);
  Root.SendToTreeView(TreeView1);
end;

procedure TFormMain.Button4Click(Sender: TObject);
begin
  if not Root.GetValue(Edit2.Text).IsEmpty then
    ShowMessage(Root.GetValue(Edit2.Text));
end;

procedure TFormMain.TreeView1Click(Sender: TObject);
var
  s: TTreeNode;
  names: string;
begin
  names := '';
  s := TreeView1.Selected;

  while s <> nil do
  begin
    names := s.Text + '|' + names;
    s := s.Parent;
  end;

  names := names.Remove(names.Length - 1);

  if names.IndexOf('=') = -1 then
    names := copy(names, 1)
  else
    names := copy(names, 1, names.IndexOf('=') + 1);
  Edit1.Text := names;

  Edit2.Text := names.Replace('|', '\').Replace('{', '').Replace('}', '')
    .Replace('[', '').Replace(']', '').Replace('=', '\').Replace('Root\', '');

  names := names.Replace('|[', '.JArray[''');
  names := names.Replace(']', ''']');

  names := names.Replace('|{', '.JObjs[''');
  names := names.Replace('}', ''']');

  names := names.Replace('|', '.JParams[''');
  names := names.Replace('=', ''']');

  Edit1.Text := names;
  Edit1.CopyToClipboard;
end;

end.
